//
// Created by vlad on 1/7/24.
//

#ifndef ACTIVEMQCLIENTLIB_ACTIVEMQRECEIVER_H
#define ACTIVEMQCLIENTLIB_ACTIVEMQRECEIVER_H

#include <string>
#include <mutex>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <activemq/transport/DefaultTransportListener.h>
#include <activemq/core/ActiveMQConnection.h>
#include <cms/ExceptionListener.h>
#include <cms/MessageListener.h>
#include <cms/TextMessage.h>
#pragma GCC diagnostic pop

#include <boost/signals2.hpp>

namespace receiver
{
/// Receiver class for ActiveMQ. Creates session on construction and listens asynchronously to messages.
/// Processes text messages only. Filters out empty messages.
class ActiveMqReceiver
        : public cms::ExceptionListener
                , public cms::MessageListener
                , public activemq::transport::DefaultTransportListener
{
public:
    ActiveMqReceiver(std::unique_ptr<cms::Connection> connection, const std::string &queueName);

    ~ActiveMqReceiver();

    /// callback on message enqueue (handled internally)
    /// \param message cms TextMessage only is processed, all other types are ignored
    void onMessage(const cms::Message *message) override;

    /// callback on exception (handled internally)
    /// \param ex client exception during session/connection errors
    void onException(const cms::CMSException &ex AMQCPP_UNUSED) override;

    ///  callback to run after message is received
    /// \param callback
    void attachOnMessageReceivedCallback(std::function<void(std::string)> &&callback);

    /// callback on connection interrupt (interrupt logging only)
    void transportInterrupted() override;

    /// callback on connection resume (resume logging only)
    void transportResumed() override;

    /// closes connection and session to queue (has to be called explicitly when done)
    void close();

    /// shows how many messages were processed
    std::uint64_t msgCount() const;

    ActiveMqReceiver(ActiveMqReceiver &other) = delete;
    ActiveMqReceiver(ActiveMqReceiver &&other) = delete;
    ActiveMqReceiver& operator=(ActiveMqReceiver &other) = delete;
    ActiveMqReceiver& operator=(ActiveMqReceiver &&other) = delete;

private:
    std::mutex m_operationMutex;
    std::uint64_t m_msgCount{0};
    std::string m_lastMsg;
    boost::signals2::signal<void(const std::string&)> s_onMessageReceived;
    std::vector<boost::signals2::connection> m_onMessageReceivedConnections;
    std::shared_ptr<cms::Session> m_session;
    std::shared_ptr<cms::Destination> m_queue;
    std::shared_ptr<cms::MessageConsumer> m_consumer;
    std::unique_ptr<cms::Connection> m_connection;
};
}

#endif //ACTIVEMQCLIENTLIB_ACTIVEMQRECEIVER_H
