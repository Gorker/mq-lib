//
// Created by vlad on 1/7/24.
//

#ifndef ACTIVEMQCLIENTLIB_ACTIVEMQSENDER_H
#define ACTIVEMQCLIENTLIB_ACTIVEMQSENDER_H
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <decaf/lang/Thread.h>
#include <decaf/lang/Runnable.h>
#include <activemq/transport/DefaultTransportListener.h>
#include <activemq/core/ActiveMQConnection.h>
#include <activemq/library/ActiveMQCPP.h>
#include <cms/ExceptionListener.h>
#include <cms/MessageListener.h>
#include <cms/TextMessage.h>
#pragma GCC diagnostic pop

#include <mutex>
#include <boost/algorithm/string.hpp>
#include <queue>
#include <iomanip>

#include "messages.h"

namespace sender
{
/// Message sender class for ActiveMQ. Creates session on construction and waits asynchronously
/// for incoming messages to send.
/// Processes text messages with a "text" prefix only (prefix is later filtered before the message is enqueued).
class ActiveMqSender : public decaf::lang::Runnable
{
public:
    ActiveMqSender(std::unique_ptr<cms::Connection> connection,
                   const std::string &queueName);

    ~ActiveMqSender();

    /// send a text message to broker
    /// \param msg any type of string with a "text" prefix
    /// \return status, if the message was accepted or not, and what is the reason if not.
    sender::messages::MessageAcceptanceStatus sendMessage(const std::string &msg);

    /// closes connection and session to queue (has to be called explicitly when done)
    void close();

    ActiveMqSender(ActiveMqSender &other) = delete;
    ActiveMqSender(ActiveMqSender &&other) = delete;
    ActiveMqSender& operator=(ActiveMqSender &other) = delete;
    ActiveMqSender& operator=(ActiveMqSender &&other) = delete;

private:
    void run() override;

    std::queue<std::string> m_pendingMsgQueue;
    std::mutex m_operationMutex;
    bool m_run{false};
    std::shared_ptr<decaf::lang::Thread> m_runningThread;
    std::unique_ptr<cms::Session> m_session;
    std::unique_ptr<cms::Destination> m_queue;
    std::unique_ptr<cms::MessageProducer> m_producer;
    std::unique_ptr<cms::Connection> m_connection;
};
}

#endif //ACTIVEMQCLIENTLIB_ACTIVEMQSENDER_H
