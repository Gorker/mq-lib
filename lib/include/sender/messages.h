//
// Created by vlad on 1/8/24.
//

#include <string>

#ifndef ACTIVEMQCLIENTLIB_MESSAGES_H
#define ACTIVEMQCLIENTLIB_MESSAGES_H

namespace sender::messages
{
/// enum to define if a message was accepted for traffic or not
enum class MessageAcceptanceCode
{
    Success = 0,
    Fail = 1
};

/// struct to define the status of a sent message, and if it was not, states the reason
struct MessageAcceptanceStatus
{
    std::string reason;
    MessageAcceptanceCode code;
};
}

#endif //ACTIVEMQCLIENTLIB_MESSAGES_H

