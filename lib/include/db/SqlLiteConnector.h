//
// Created by vlad on 1/6/24.
//

#ifndef ACTIVEMQCLIENTLIB_SQLLITEBROKER_H
#define ACTIVEMQCLIENTLIB_SQLLITEBROKER_H

#include <string>

#include <sqlite_modern_cpp.h>

namespace db::sql
{
/// SQL Lite v3 DB connector used for persisting new messages into a message database with a given name
/// Database fields are strict: id (auto generated), date (auto generated) and body (user message to persist)
class SqlLiteConnector
{
public:
    SqlLiteConnector() = delete;

    explicit SqlLiteConnector(const std::string &dbName);

    /// persist new message into database
    /// \param msg message to insert
    void insert(const std::string &msg);

    /// fetch message from database by id
    /// \param msgId  message id to fetch
    /// \return message
    std::string select(std::uint32_t msgId);
private:
    std::string datetime();

    sqlite::database m_db;
};
}

#endif //ACTIVEMQCLIENTLIB_SQLLITEBROKER_H
