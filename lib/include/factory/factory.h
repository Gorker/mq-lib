//
// Created by vlad on 1/7/24.
//

#ifndef ACTIVEMQCLIENTLIB_FACTORY_H
#define ACTIVEMQCLIENTLIB_FACTORY_H
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <activemq/transport/DefaultTransportListener.h>
#include <activemq/core/ActiveMQConnectionFactory.h>
#pragma GCC diagnostic pop

namespace factory
{
///  Class to for library initialization and component generation
class ClientFactory
{
public:
    ClientFactory();

    ~ClientFactory();

    /// Create an instance of a ActiveMqComponent (either ActiveMqReceiver or ActiveMqSender)
    /// with an active connection to the MQ
    /// \param brokerUri : valid uri to access the ActiveMQ
    /// \excetion std::runtime_error in case a connection could not be established after 10 secs
    /// \return ActiveMqReceiver object
    template<class ActiveMqComp>
    inline ActiveMqComp createComponent(const std::string &brokerUri, const std::string &queueName);

    ClientFactory(ClientFactory &other) = delete;
    ClientFactory(ClientFactory &&other) = delete;
    ClientFactory& operator=(ClientFactory &other) = delete;
    ClientFactory& operator=(ClientFactory &&other) = delete;
};

template<class ActiveMqComp>
ActiveMqComp ClientFactory::createComponent(const std::string &brokerUri, const std::string &queueName) {
    activemq::core::ActiveMQConnectionFactory connectionFactory =
            activemq::core::ActiveMQConnectionFactory(brokerUri);

    std::unique_ptr<cms::Connection> connPtr;
    size_t retries{10};
    do
    {
        try
        {
            connPtr.reset(connectionFactory.createConnection());
        }
        catch (const cms::CMSException &e)
        {
            std::cout << "Active MQ at "<< brokerUri
                      <<" is not reachable, waiting for connection to be established..." << std::endl;
            --retries;
        }

    } while (!connPtr && retries > 0);

    if (!connPtr)
    {
        throw std::runtime_error(
                "Connection with Active MQ at address: " + brokerUri + " cannot be established");
    }

    return ActiveMqComp(std::move(connPtr), queueName);
}
}

#endif //ACTIVEMQCLIENTLIB_FACTORY_H
