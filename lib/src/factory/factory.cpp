//
// Created by vlad on 1/9/24.
//

#include "factory/factory.h"

#include <activemq/library/ActiveMQCPP.h>

namespace factory
{
ClientFactory::ClientFactory()
{
    activemq::library::ActiveMQCPP::initializeLibrary();
}

ClientFactory::~ClientFactory()
{
    activemq::library::ActiveMQCPP::shutdownLibrary();
}
}

