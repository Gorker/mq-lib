//
// Created by vlad on 1/7/24.
//

#include "receiver/ActiveMqReceiver.h"

namespace receiver
{
ActiveMqReceiver::ActiveMqReceiver(std::unique_ptr<cms::Connection> connection,
                                   const std::string &queueName)
{
    if (!connection)
    {
        throw std::runtime_error("Cannot create a receiver. Connection given is null");
    }

    auto mqConn = dynamic_cast<activemq::core::ActiveMQConnection*>(connection.get());
    if (!mqConn)
    {
        throw std::runtime_error("Cannot create a receiver. Connection given is not ActiveMQConnection");
    }

    m_connection = std::move(connection);
    try
    {
        mqConn->addTransportListener(this);
        mqConn->start();
        mqConn->setExceptionListener(this);
        m_session.reset(mqConn->createSession(cms::Session::AcknowledgeMode::CLIENT_ACKNOWLEDGE));
        m_queue.reset(m_session->createQueue(queueName));
        m_consumer.reset(m_session->createConsumer(m_queue.get()));
        m_consumer->setMessageListener(this);
    }
    catch (const cms::CMSException &e)
    {
        close();
        std::cerr << "Could not start connection due to: " << e.what() << std::endl;
        throw e;
    }
}

void ActiveMqReceiver::onMessage(const cms::Message *message) {
    std::unique_lock lock(m_operationMutex);
    auto textMsg = dynamic_cast<const cms::TextMessage*>(message);
    if (textMsg == nullptr)
    {
        std::cerr << "TextMessage received is not a text message!" << std::endl;
        if (message != nullptr)
        {
            message->acknowledge();
        }
        return;
    }

    try
    {
        auto msgTxt = textMsg->getText();
        if (!msgTxt.empty())
        {
            m_lastMsg = std::move(msgTxt);
            std::cout << "TextMessage" << std::to_string(++m_msgCount) << " received" << std::endl;
            lock.unlock();
            s_onMessageReceived(m_lastMsg);
        }
        message->acknowledge();

    } catch (cms::CMSException &e) {
        e.printStackTrace();
    }
}

void ActiveMqReceiver::onException(const cms::CMSException &ex)
{
    std::cerr << "Message could not be processed due to: " << ex.what() << std::endl;
}

void ActiveMqReceiver::attachOnMessageReceivedCallback(std::function<void(std::string)> &&callback)
{
    m_onMessageReceivedConnections.push_back(s_onMessageReceived.connect(callback));
}

void ActiveMqReceiver::close()
{
    std::lock_guard lock(m_operationMutex);

    if (m_queue)
    {
        try
        {
            m_queue.reset();
        }
        catch (const cms::CMSException&)
        {
            m_queue = nullptr;
        }
    }
    if (m_consumer)
    {
        try
        {
            m_consumer->close();
            m_consumer.reset();
        }
        catch (const cms::CMSException&)
        {
            m_consumer = nullptr;
        }
    }
    if(m_session)
    {
        try
        {
            m_session->close();
        }
        catch (const cms::CMSException&)
        {
        }
    }
    if(m_connection)
    {
        try
        {
            m_connection->close();
        }
        catch (const cms::CMSException&)
        {
        }
    }
    if(m_session)
    {
        try
        {
            m_session.reset();
        }
        catch (const cms::CMSException&)
        {
            m_session = nullptr;
        }
    }
    if(m_connection)
    {
        try
        {
            m_connection.reset();
        }
        catch (const cms::CMSException&)
        {
            m_connection = nullptr;
        }
    }
}

ActiveMqReceiver::~ActiveMqReceiver()
{
    close();
}

void ActiveMqReceiver::transportInterrupted()
{
    std::cout << "The Connection's Transport has been Interrupted." << std::endl;
}

void ActiveMqReceiver::transportResumed()
{
    std::cout << "The Connection's Transport has been Restored." << std::endl;
}

std::uint64_t ActiveMqReceiver::msgCount() const
{
    return m_msgCount;
}

}

