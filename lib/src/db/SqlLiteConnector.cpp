//
// Created by vlad on 1/7/24.
//

#include <chrono>
#include <sstream>
#include <iomanip>

#include "db/SqlLiteConnector.h"

namespace db::sql
{
SqlLiteConnector::SqlLiteConnector(const std::string &dbName) :m_db(dbName)
{
    m_db << "create table if not exists message ("
            "   id integer primary key autoincrement not null,"
            "   date datetime,"
            "   body text"
            ");";
}

void SqlLiteConnector::insert(const std::string &msg)
{
    try
    {
        m_db << "insert into message (date, body) values (?,?)" << datetime() << msg;
    }
    catch (const sqlite::sqlite_exception &e)
    {
        std::cerr << "Could not insert new message to database due to :" << e.what()
                  << " error code: " << e.get_code() << std::endl;
    }
}

std::string SqlLiteConnector::datetime() {
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);

    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
    return ss.str();
}

std::string SqlLiteConnector::select(std::uint32_t msgId)
{
    std::string result;
    try
    {
        m_db << "select body from message where id = ?" << msgId >> result;
    }
    catch (const sqlite::sqlite_exception &e)
    {
        // no row, return empty string
    }

    return result;
}
}

