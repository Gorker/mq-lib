//
// Created by vlad on 1/7/24.
//

#include "sender/ActiveMqSender.h"

namespace sender
{
ActiveMqSender::ActiveMqSender(std::unique_ptr<cms::Connection> connection, const std::string &queueName) {
    if (!connection)
    {
        throw std::runtime_error("Cannot create a producer. Connection given is null");
    }
    auto mqConn = dynamic_cast<activemq::core::ActiveMQConnection*>(connection.get());
    if (!mqConn)
    {
        throw std::runtime_error("Cannot create a producer. Connection given is not an ActiveMQConnection");
    }
    m_connection = std::move(connection);
    try
    {
        mqConn->start();
        m_session.reset(mqConn->createSession(cms::Session::CLIENT_ACKNOWLEDGE));
        m_queue.reset(m_session->createQueue(queueName));
        m_producer.reset(m_session->createProducer(m_queue.get()));
        m_producer->setDeliveryMode(cms::DeliveryMode::NON_PERSISTENT);
        m_runningThread = std::make_unique<decaf::lang::Thread>(this);
        m_run = true;
        m_runningThread->start();
    }
    catch (const cms::CMSException &e)
    {
        close();
        std::cerr << "Could not start connection due to: " << e.what() << std::endl;
        throw e;
    }
}

ActiveMqSender::~ActiveMqSender() {
    close();
}

sender::messages::MessageAcceptanceStatus ActiveMqSender::sendMessage(const std::string &msg) {
    const std::string prefix{"text"};
    if (!boost::algorithm::starts_with(msg, prefix))
    {
        auto reason = "Missing prefix: '" + prefix + "'";
        return {std::move(reason), sender::messages::MessageAcceptanceCode::Fail};
    }
    {
        std::lock_guard lock(m_operationMutex);
        auto msgNoPrefix = boost::algorithm::replace_all_copy(msg, prefix, "");
        m_pendingMsgQueue.push(std::move(msgNoPrefix));
    }

    return{.code = sender::messages::MessageAcceptanceCode::Success};
}

void ActiveMqSender::close() {
    m_run = false;
    if (m_runningThread)
    {
        m_runningThread->join();
        m_runningThread.reset();
    }

    std::lock_guard lock(m_operationMutex);
    if (m_queue)
    {
        try
        {
            m_queue.reset();
        }
        catch (const cms::CMSException&)
        {
            m_queue = nullptr;
        }
    }
    if (m_producer)
    {
        try
        {
            m_producer->close();
            m_producer.reset();
        }
        catch (const cms::CMSException&)
        {
            m_producer = nullptr;
        }
    }
    if(m_session)
    {
        try
        {
            m_session->close();
        }
        catch (const cms::CMSException&)
        {
        }
    }
    if(m_connection)
    {
        try
        {
            m_connection->close();
        }
        catch (const cms::CMSException&)
        {
        }
    }
    if(m_session)
    {
        try
        {
            m_session.reset();
        }
        catch (const cms::CMSException&)
        {
            m_session = nullptr;
        }
    }
    if(m_connection)
    {
        try
        {
            m_connection.reset();
        }
        catch (const cms::CMSException&)
        {
            m_connection = nullptr;
        }
    }
}

void ActiveMqSender::run() {

    while (m_run)
    {
        std::unique_lock lock(m_operationMutex);
        while (!m_pendingMsgQueue.empty())
        {
            auto txt = std::move(m_pendingMsgQueue.front());
            m_pendingMsgQueue.pop();
            try
            {
                std::unique_ptr<cms::TextMessage> message(m_session->createTextMessage(txt));
                m_producer->send(message.get());
            }
            catch (cms::CMSException &ex)
            {
                std::cerr << "Could not send message due to: " << ex.what() << std::endl;
            }
        }
        lock.unlock();
        decaf::lang::Thread::currentThread()->sleep(1000);
    }
}
}

