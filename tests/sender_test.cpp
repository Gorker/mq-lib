//
// Created by vlad on 1/9/24.
//
#include <thread>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <activemq/transport/mock/MockTransportFactory.h>
#include <activemq/transport/TransportRegistry.h>
#pragma GCC diagnostic pop

#include "mocks/CmsMock.h"
#include "sender/ActiveMqSender.h"
#include "factory/factory.h"

TEST(SenderTests, nullConnectionsReceived)
{
    EXPECT_THROW(sender::ActiveMqSender(nullptr, ""), std::runtime_error);

    auto conn = std::make_unique<CmsBaseConnectionMock>();

    EXPECT_THROW(sender::ActiveMqSender(std::move(conn), ""), std::runtime_error);
}

TEST(SenderTests, givenCorrectConnectionActiveSenderCreated)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    EXPECT_NO_THROW(sender::ActiveMqSender(std::move(conn), "test"));
}

TEST(SenderTests, SenderClosedProperly)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    EXPECT_NO_THROW(sender::ActiveMqSender(std::move(conn), "test").close());
}

TEST(SenderTests, onCmsExceptionDuringCreation)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionThrowMock>(transport, properties);
    EXPECT_THROW(sender::ActiveMqSender(std::move(conn), "test"), cms::CMSException);
}

TEST(SenderTests, sendEmptyMessage)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto sndr = sender::ActiveMqSender(std::move(conn), "test");
    auto status = sndr.sendMessage("");
    EXPECT_EQ(status.code, sender::messages::MessageAcceptanceCode::Fail);
}

TEST(SenderTests, sendTextMessageNoPrefix)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto sndr = sender::ActiveMqSender(std::move(conn), "test");
    auto status = sndr.sendMessage("test");
    EXPECT_EQ(status.code, sender::messages::MessageAcceptanceCode::Fail);
}

TEST(SenderTests, sendCorrectTextMessage)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto sndr = sender::ActiveMqSender(std::move(conn), "test");
    auto status = sndr.sendMessage("text test");
    EXPECT_EQ(status.code, sender::messages::MessageAcceptanceCode::Success);
}