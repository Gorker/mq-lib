//
// Created by vlad on 1/10/24.
//

#ifndef ACTIVEMQCLIENTLIB_CMSMOCK_H
#define ACTIVEMQCLIENTLIB_CMSMOCK_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <cms/Connection.h>
#include <activemq/core/ActiveMQConnection.h>
#pragma GCC diagnostic pop

class MsgMock : public cms::Message
{
public:
    Message *clone() const override {
        return nullptr;
    }

    void acknowledge() const override {

    }

    void clearBody() override {

    }

    void clearProperties() override {

    }

    std::vector<std::string> getPropertyNames() const override {
        return std::vector<std::string>();
    }

    bool propertyExists(const std::string &name) const override {
        return false;
    }

    ValueType getPropertyValueType(const std::string &name) const override {
        return SHORT_TYPE;
    }

    bool getBooleanProperty(const std::string &name) const override {
        return false;
    }

    unsigned char getByteProperty(const std::string &name) const override {
        return 0;
    }

    double getDoubleProperty(const std::string &name) const override {
        return 0;
    }

    float getFloatProperty(const std::string &name) const override {
        return 0;
    }

    int getIntProperty(const std::string &name) const override {
        return 0;
    }

    long long int getLongProperty(const std::string &name) const override {
        return 0;
    }

    short getShortProperty(const std::string &name) const override {
        return 0;
    }

    std::string getStringProperty(const std::string &name) const override {
        return std::string();
    }

    void setBooleanProperty(const std::string &name, bool value) override {

    }

    void setByteProperty(const std::string &name, unsigned char value) override {

    }

    void setDoubleProperty(const std::string &name, double value) override {

    }

    void setFloatProperty(const std::string &name, float value) override {

    }

    void setIntProperty(const std::string &name, int value) override {

    }

    void setLongProperty(const std::string &name, long long int value) override {

    }

    void setShortProperty(const std::string &name, short value) override {

    }

    void setStringProperty(const std::string &name, const std::string &value) override {

    }

    std::string getCMSCorrelationID() const override {
        return std::string();
    }

    void setCMSCorrelationID(const std::string &correlationId) override {

    }

    int getCMSDeliveryMode() const override {
        return 0;
    }

    void setCMSDeliveryMode(int mode) override {

    }

    const cms::Destination *getCMSDestination() const override {
        return nullptr;
    }

    void setCMSDestination(const cms::Destination *destination) override {

    }

    long long int getCMSExpiration() const override {
        return 0;
    }

    void setCMSExpiration(long long int expireTime) override {

    }

    std::string getCMSMessageID() const override {
        return std::string();
    }

    void setCMSMessageID(const std::string &id) override {

    }

    int getCMSPriority() const override {
        return 0;
    }

    void setCMSPriority(int priority) override {

    }

    bool getCMSRedelivered() const override {
        return false;
    }

    void setCMSRedelivered(bool redelivered) override {

    }

    const cms::Destination *getCMSReplyTo() const override {
        return nullptr;
    }

    void setCMSReplyTo(const cms::Destination *destination) override {

    }

    long long int getCMSTimestamp() const override {
        return 0;
    }

    void setCMSTimestamp(long long int timeStamp) override {

    }

    std::string getCMSType() const override {
        return std::string();
    }

    void setCMSType(const std::string &type) override {

    }

};

class TxtMsgMock : public cms::TextMessage
{
public:
    Message *clone() const override {
        return nullptr;
    }

    void acknowledge() const override {

    }

    void clearBody() override {

    }

    void clearProperties() override {

    }

    std::vector<std::string> getPropertyNames() const override {
        return std::vector<std::string>();
    }

    bool propertyExists(const std::string &name) const override {
        return false;
    }

    ValueType getPropertyValueType(const std::string &name) const override {
        return SHORT_TYPE;
    }

    bool getBooleanProperty(const std::string &name) const override {
        return false;
    }

    unsigned char getByteProperty(const std::string &name) const override {
        return 0;
    }

    double getDoubleProperty(const std::string &name) const override {
        return 0;
    }

    float getFloatProperty(const std::string &name) const override {
        return 0;
    }

    int getIntProperty(const std::string &name) const override {
        return 0;
    }

    long long int getLongProperty(const std::string &name) const override {
        return 0;
    }

    short getShortProperty(const std::string &name) const override {
        return 0;
    }

    std::string getStringProperty(const std::string &name) const override {
        return std::string();
    }

    void setBooleanProperty(const std::string &name, bool value) override {

    }

    void setByteProperty(const std::string &name, unsigned char value) override {

    }

    void setDoubleProperty(const std::string &name, double value) override {

    }

    void setFloatProperty(const std::string &name, float value) override {

    }

    void setIntProperty(const std::string &name, int value) override {

    }

    void setLongProperty(const std::string &name, long long int value) override {

    }

    void setShortProperty(const std::string &name, short value) override {

    }

    void setStringProperty(const std::string &name, const std::string &value) override {

    }

    std::string getCMSCorrelationID() const override {
        return std::string();
    }

    void setCMSCorrelationID(const std::string &correlationId) override {

    }

    int getCMSDeliveryMode() const override {
        return 0;
    }

    void setCMSDeliveryMode(int mode) override {

    }

    const cms::Destination *getCMSDestination() const override {
        return nullptr;
    }

    void setCMSDestination(const cms::Destination *destination) override {

    }

    long long int getCMSExpiration() const override {
        return 0;
    }

    void setCMSExpiration(long long int expireTime) override {

    }

    std::string getCMSMessageID() const override {
        return std::string();
    }

    void setCMSMessageID(const std::string &id) override {

    }

    int getCMSPriority() const override {
        return 0;
    }

    void setCMSPriority(int priority) override {

    }

    bool getCMSRedelivered() const override {
        return false;
    }

    void setCMSRedelivered(bool redelivered) override {

    }

    const cms::Destination *getCMSReplyTo() const override {
        return nullptr;
    }

    void setCMSReplyTo(const cms::Destination *destination) override {

    }

    long long int getCMSTimestamp() const override {
        return 0;
    }

    void setCMSTimestamp(long long int timeStamp) override {

    }

    std::string getCMSType() const override {
        return std::string();
    }

    void setCMSType(const std::string &type) override {

    }

    std::string getText() const override {
        return "This is a test";
    }

    void setText(const char *msg) override {

    }

    void setText(const std::string &msg) override {

    }

};
class CmsDestinationMock : public cms::Queue
{
public:
    cms::Destination::DestinationType getDestinationType() const override {
        return TEMPORARY_QUEUE;
    }

    cms::Destination *clone() const override {
        return nullptr;
    }

    void copy(const Destination &source) override {

    }

    bool equals(const Destination &other) const override {
        return false;
    }

    const cms::CMSProperties &getCMSProperties() const override {
        return *m_props;
    }

    std::string getQueueName() const override {
        return {};
    }

public:
    std::unique_ptr<cms::CMSProperties> m_props;
};

class CmsConsumerMock : public cms::MessageConsumer
{
public:
    void close() override {

    }

    cms::Message *receive() override {
        return nullptr;
    }

    cms::Message *receive(int millisecs) override {
        return nullptr;
    }

    cms::Message *receiveNoWait() override {
        return nullptr;
    }

    void setMessageListener(cms::MessageListener *listener) override {

    }

    cms::MessageListener *getMessageListener() const override {
        return nullptr;
    }

    std::string getMessageSelector() const override {
        return std::string();
    }

    void setMessageTransformer(cms::MessageTransformer *transformer) override {

    }

    cms::MessageTransformer *getMessageTransformer() const override {
        return nullptr;
    }

    void setMessageAvailableListener(cms::MessageAvailableListener *listener) override {

    }

    cms::MessageAvailableListener *getMessageAvailableListener() const override {
        return nullptr;
    }

    void start() override {

    }

    void stop() override {

    }

public:

};

class CmsProducerMock : public cms::MessageProducer
{
public:
    void close() override {

    }

    void send(cms::Message *message) override {

    }

    void send(cms::Message *message, cms::AsyncCallback *onComplete) override {

    }

    void send(cms::Message *message, int deliveryMode, int priority, long long int timeToLive) override {

    }

    void send(cms::Message *message, int deliveryMode, int priority, long long int timeToLive,
              cms::AsyncCallback *onComplete) override {

    }

    void send(const cms::Destination *destination, cms::Message *message) override {

    }

    void send(const cms::Destination *destination, cms::Message *message, cms::AsyncCallback *onComplete) override {

    }

    void send(const cms::Destination *destination, cms::Message *message, int deliveryMode, int priority,
              long long int timeToLive) override {

    }

    void send(const cms::Destination *destination, cms::Message *message, int deliveryMode, int priority,
              long long int timeToLive, cms::AsyncCallback *onComplete) override {

    }

    void setDeliveryMode(int mode) override {

    }

    int getDeliveryMode() const override {
        return 0;
    }

    void setDisableMessageID(bool value) override {

    }

    bool getDisableMessageID() const override {
        return false;
    }

    void setDisableMessageTimeStamp(bool value) override {

    }

    bool getDisableMessageTimeStamp() const override {
        return false;
    }

    void setPriority(int priority) override {

    }

    int getPriority() const override {
        return 0;
    }

    void setTimeToLive(long long int time) override {

    }

    long long int getTimeToLive() const override {
        return 0;
    }

    void setMessageTransformer(cms::MessageTransformer *transformer) override {

    }

    cms::MessageTransformer *getMessageTransformer() const override {
        return nullptr;
    }

};

class CmsSessionMock : public cms::Session
{
public:
    void commit() override {

    }

    void close() override {

    }

    void rollback() override {

    }

    void recover() override {

    }

    cms::MessageConsumer *createConsumer(const cms::Destination *destination) override {
        return new CmsConsumerMock();
    }

    cms::MessageConsumer *createConsumer(const cms::Destination *destination, const std::string &selector) override {
        return new CmsConsumerMock();
    }

    cms::MessageConsumer *
    createConsumer(const cms::Destination *destination, const std::string &selector, bool noLocal) override {
        return new CmsConsumerMock();
    }

    cms::MessageConsumer *
    createDurableConsumer(const cms::Topic *destination, const std::string &name, const std::string &selector,
                          bool noLocal) override {
        return nullptr;
    }

    cms::MessageProducer *createProducer(const cms::Destination *destination) override {
        return new CmsProducerMock();
    }

    cms::QueueBrowser *createBrowser(const cms::Queue *queue) override {
        return nullptr;
    }

    cms::QueueBrowser *createBrowser(const cms::Queue *queue, const std::string &selector) override {
        return nullptr;
    }

    cms::Queue *createQueue(const std::string &queueName) override {
        return new CmsDestinationMock();
    }

    cms::Topic *createTopic(const std::string &topicName) override {
        return nullptr;
    }

    cms::TemporaryQueue *createTemporaryQueue() override {
        return nullptr;
    }

    cms::TemporaryTopic *createTemporaryTopic() override {
        return nullptr;
    }

    cms::Message *createMessage() override {
        return nullptr;
    }

    cms::BytesMessage *createBytesMessage() override {
        return nullptr;
    }

    cms::BytesMessage *createBytesMessage(const unsigned char *bytes, int bytesSize) override {
        return nullptr;
    }

    cms::StreamMessage *createStreamMessage() override {
        return nullptr;
    }

    cms::TextMessage *createTextMessage() override {
        return nullptr;
    }

    cms::TextMessage *createTextMessage(const std::string &text) override {
        return nullptr;
    }

    cms::MapMessage *createMapMessage() override {
        return nullptr;
    }

    AcknowledgeMode getAcknowledgeMode() const override {
        return SESSION_TRANSACTED;
    }

    bool isTransacted() const override {
        return false;
    }

    void unsubscribe(const std::string &name) override {

    }

    void setMessageTransformer(cms::MessageTransformer *transformer) override {

    }

    cms::MessageTransformer *getMessageTransformer() const override {
        return nullptr;
    }

    void start() override {

    }

    void stop() override {

    };
};

class CmsBaseConnectionMock : public cms::Connection
{
public:
    MOCK_METHOD(void, start, (), (override));
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(void, stop, (), (override));
    MOCK_METHOD(void, setClientID, (const std::string&), (override));
    MOCK_METHOD(std::string, getClientID, (), (const, override));
    MOCK_METHOD(cms::ConnectionMetaData*, getMetaData, (), (const, override));
    MOCK_METHOD(void, setExceptionListener, (cms::ExceptionListener*), (override));
    MOCK_METHOD(cms::ExceptionListener*, getExceptionListener, (), (const, override));
    MOCK_METHOD(void, setMessageTransformer, (cms::MessageTransformer*), (override));
    MOCK_METHOD(cms::MessageTransformer*, getMessageTransformer, (), (const, override));
    MOCK_METHOD(CmsSessionMock*, createSession, (cms::Session::AcknowledgeMode), (override));
    MOCK_METHOD(CmsSessionMock*, createSession, (), (override));
};

class AMQCPP_API CmsConnectionMock : public activemq::core::ActiveMQConnection
{
public:
    CmsConnectionMock(const decaf::lang::Pointer<activemq::transport::Transport> &transport,
                      const decaf::lang::Pointer<decaf::util::Properties> &properties) : ActiveMQConnection(transport,
                                                                                                            properties)
    {

    }

    MOCK_METHOD(void, addTransportLayer, (activemq::transport::TransportListener*), ());
    MOCK_METHOD(void, start, (), (override));
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(void, setExceptionListener, (cms::ExceptionListener*), (override));

    CmsSessionMock* createSession(cms::Session::AcknowledgeMode) override
    {
       return new CmsSessionMock();
    }
};

class AMQCPP_API  CmsConnectionThrowMock : public activemq::core::ActiveMQConnection
{
public:
    CmsConnectionThrowMock(const decaf::lang::Pointer<activemq::transport::Transport> &transport,
                      const decaf::lang::Pointer<decaf::util::Properties> &properties) : ActiveMQConnection(transport,
                                                                                                            properties)
    {

    }

    CmsSessionMock* createSession(cms::Session::AcknowledgeMode) override
    {
        throw cms::CMSException();
    }

};
#endif //ACTIVEMQCLIENTLIB_CMSMOCK_H
