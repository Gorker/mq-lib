//
// Created by vlad on 1/9/24.
//
#include <thread>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <activemq/transport/mock/MockTransportFactory.h>
#include <activemq/transport/TransportRegistry.h>
#include <activemq/core/ActiveMQConnectionFactory.h>

#pragma GCC diagnostic pop

#include "mocks/CmsMock.h"
#include "receiver/ActiveMqReceiver.h"
#include "factory/factory.h"


TEST(ReceiverTests, nullConnectionsReceived)
{
    EXPECT_THROW(receiver::ActiveMqReceiver(nullptr, ""), std::runtime_error);

    auto conn = std::make_unique<CmsBaseConnectionMock>();

    EXPECT_THROW(receiver::ActiveMqReceiver(std::move(conn), ""), std::runtime_error);
}

TEST(ReceiverTests, givenCorrectConnectionActiveReceiverCreated)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    EXPECT_NO_THROW(receiver::ActiveMqReceiver(std::move(conn), "test"));
}

TEST(ReceiverTests, ReceiverClosedProperly)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    EXPECT_NO_THROW(receiver::ActiveMqReceiver(std::move(conn), "test").close());
}

TEST(ReceiverTests, onCmsExceptionDuringCreation)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    auto transport = factory.create(uri);
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto conn = std::make_unique<CmsConnectionThrowMock>(transport, properties);
    EXPECT_THROW(receiver::ActiveMqReceiver(std::move(conn), "test"), cms::CMSException);
}

TEST(ReceiverTests, onEmptyMessage)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto rcv = receiver::ActiveMqReceiver(std::move(conn), "test");
    std::unique_ptr<cms::BytesMessage> msg;
    rcv.attachOnMessageReceivedCallback([](std::string msg){
        FAIL();
    });
    rcv.onMessage(nullptr);
    EXPECT_EQ(rcv.msgCount(), 0);
}

TEST(ReceiverTests, onNonTextMessage)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto rcv = receiver::ActiveMqReceiver(std::move(conn), "test");
    auto msg = std::make_unique<MsgMock>();
    rcv.attachOnMessageReceivedCallback([](std::string msg){
        FAIL();
    });
    rcv.onMessage(nullptr);
    EXPECT_EQ(rcv.msgCount(), 0);
}

TEST(ReceiverTests, onTextMessage)
{
    ::testing::FLAGS_gmock_verbose = "error";
    auto client = factory::ClientFactory();
    auto uri = decaf::net::URI("mock://mock?wireFormat=openwire");
    auto factory = activemq::transport::mock::MockTransportFactory();
    decaf::lang::Pointer<decaf::util::Properties> properties(new decaf::util::Properties());
    auto transport = factory.create(uri);
    auto conn = std::make_unique<CmsConnectionMock>(transport, properties);
    auto rcv = receiver::ActiveMqReceiver(std::move(conn), "test");
    auto msg = std::make_unique<TxtMsgMock>();
    bool callbackDone = false;
    rcv.attachOnMessageReceivedCallback([&callbackDone](std::string msg){
        EXPECT_EQ(msg, "This is a test");
        callbackDone = true;
    });
    rcv.onMessage(msg.get());
    int retries = 10;
    while (!callbackDone && retries > 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        --retries;
    }

    EXPECT_EQ(rcv.msgCount(), 1);
}
