//
// Created by vlad on 1/9/24.
//
#include <gtest/gtest.h>

#include "db/SqlLiteConnector.h"
#include "factory/factory.h"

TEST(FactoryTests, dbCreatedAndRecordInserted)
{
    auto client = factory::ClientFactory();
    auto dbConn = db::sql::SqlLiteConnector(":memory:");
    dbConn.insert("Hi");
    auto msg= dbConn.select(1);
    EXPECT_EQ(msg, "Hi");
}