//
// Created by vlad on 1/9/24.
//

// the factory can mock an connection, therefore I can just use it by giving it a mock address
#include <thread>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <activemq/transport/mock/MockTransportFactory.h>
#include <activemq/transport/TransportRegistry.h>

#pragma GCC diagnostic pop

#include "mocks/CmsMock.h"
#include "sender/ActiveMqSender.h"
#include "receiver/ActiveMqReceiver.h"
#include "factory/factory.h"

TEST(FactoryTests, emptyUri)
{
    auto client = factory::ClientFactory();

    EXPECT_THROW(client.createComponent<sender::ActiveMqSender>("", "test"), std::runtime_error);

    EXPECT_THROW(client.createComponent<receiver::ActiveMqReceiver>("", "test"), std::runtime_error);
}

TEST(FactoryTests, SenderAndReceiverConstructed)
{
    auto client = factory::ClientFactory();
    auto sndr = client.createComponent<sender::ActiveMqSender>("mock://127.0.0.1:12345?wireFormat=openwire", "test");
    auto rcv = client.createComponent<receiver::ActiveMqReceiver>("mock://127.0.0.1:12345?wireFormat=openwire", "test");
}