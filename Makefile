build:
	mkdir -p build/ ; cd build/ ; cmake .. ; cmake --build .

clean:
	rm -rf build

run_example:
	./build/app/app

test:
	./build/tests/tests