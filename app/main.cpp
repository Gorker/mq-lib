//
// Created by vlad on 1/8/24.
//

#include <thread>

#include "factory/factory.h"
#include "sender/ActiveMqSender.h"
#include "receiver/ActiveMqReceiver.h"
#include "db/SqlLiteConnector.h"

int main()
{
    auto factory = factory::ClientFactory();

    auto dbConnector = db::sql::SqlLiteConnector("messages.db3");

    std::uint32_t numOfReceivedMsgs{0};

    bool persistanceToDbDone = false;

    const std::string brokerUri = "tcp://127.0.0.1:61616";
    const std::string queueName = "APP";

    // create receiver
    auto activeReceiver = factory.createComponent<receiver::ActiveMqReceiver>(brokerUri, queueName);

    activeReceiver.attachOnMessageReceivedCallback(
    [&dbConnector, &numOfReceivedMsgs, &persistanceToDbDone](std::string msg){
                std::cout << "Message " << msg << " received and being sent to database" << std::endl;
                dbConnector.insert(msg);
                ++numOfReceivedMsgs;
                if (numOfReceivedMsgs >= 10)
                {
                    persistanceToDbDone = true;
                }
        });

    // create sender
    auto activeSender = factory.createComponent<sender::ActiveMqSender>(brokerUri, queueName);

    for (auto i = 0u; i < 10; ++i)
    {
        auto status = activeSender.sendMessage("text Hi " + std::to_string(i));
        switch(status.code)
        {
            case sender::messages::MessageAcceptanceCode::Success:
                std::cout << "Message " << std::to_string(i) << " was sent successfully" << std::endl;
                break;
            case sender::messages::MessageAcceptanceCode::Fail:
                std::cout << "Message " << std::to_string(i) << " was not sent, due to: " << status.reason << std::endl;
                break;
            default:
                std::cerr << "Code " << std::to_string(static_cast<std::uint32_t>(status.code))
                          << " is unrecognized" << std::endl;
                break;
        }
    }

    while (!persistanceToDbDone)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    activeSender.close();
    activeReceiver.close();
    return 0;
}