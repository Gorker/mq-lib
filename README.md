[README.md](README.md)

# Description
Small library for sending/receiving messages to/from ActiveMQ with SQL Lite database
persistence (implemented and tested on Ubuntu 23).

Just run: `make build`

# Project Structure
Project consists of three main parts:\
    app - small example of usage, in order to run call: `make run_example`\
    lib - the client library. All classes are documented \
    tests - tests to verify lib codebase. call `make test`\
    thirdparty - thirdparty header only packages

# Requirements:
Make sure you have an ActiveMQ up and running.

```
CXX 17 g++ (13) compiler

cmake >= 3.25
boost >= 1.74.0 (earilier versions should work as well, sudo apt-get install libboost-all-dev)
sql_modern_cpp == 3.2 (comes included in the package (under thirdparty), header only, no need to install)
ACTIVECPP == 3.9.5 (with dependencies OpenSSL (sudo apt-get install libssl-dev), APR-1 (sudo apt-get install libapr1 libapr1-dev))
GTest >= 1.13.0 (sudo apt-get install libgtest-dev)
GMock >= 1.13.0 (sudo apt-get install libgmock-dev)
Sqlite3 >= 3.42.0 (sudo apt-get install libsqlite3-dev)
```

### Install ACTIVECPP
Download the library from https://activemq.apache.org/components/cms/download/395-release
and install it.\
Verify that the apr library (libapr-1.a) is located under /usr/local/apr/lib/.
Verify that the apr include files are located under /usr/local/apr/include/.
Verify that the activemq cpp library (libactivemq-cpp.a) is located under /usr/local/lib/.
Verify that the activemq cpp include files are located under /usr/local/include/activemq-cpp-3.9.5.
If any of the paths are incorrect, please change them in the `lib/CMakeLists.txt`.

That's it, have fun!

### Using the Library
In order to use the library, please refer the the app example.
Most importantly, you must have a ClientFactory instance. Then you can use the library.